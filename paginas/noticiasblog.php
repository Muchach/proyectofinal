<h2>
	Noticias
</h2>
<hr>

<?php 
//se recoge en que pagina se está para mostrar unos resultados o otros

		if(isset($_GET['numerodePagina'])){
			$numerodePagina=$_GET['numerodePagina'];
		}else{
			$numerodePagina=0;
		}
 
//Este archivo va a recibir una accion a realizar
//si no recibe una accion, por defecto quiero LISTAR elementos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Pues dependiendo de $accion, la web hace una u otra cosa
switch($accion){
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=noticiasblog.php&accion=insertar">
				Insertar
			</a>
		</h4>
		<?php } ?>
		<hr>
		<table class="table table-striped table-hover">
		<tr>
			<th>Nombre de la noticia</th>
			<th>Acciones de la noticia</th>
		</tr>
		<?php  
		//hacemos un  listado con paginacion
		//se quiere saber el nº total de productos
		$sql="SELECT * FROM blog";
		$consulta=$conexion->query($sql);
		$numeroTotalRegistros=$consulta->num_rows;
		//se establece cuantos regiostros hay por pagina
		$numeroRegistrosPagina=5;//a mano
		
		//aqui estaba aantes el recoger en que pagina se esta que ahora está arrib		

		//hacer la consulta segun el numero de pagina para mostrar los productos de 3 en 3, en la pag 1, mostrar el producto 0,1,2 y así hasta acabar.
		$iniciolimite=$numerodePagina*$numeroRegistrosPagina;//como hay relacion entre los nº de pag y nº de registros se puede hacer en la multiplicaciond e variables, creando e que será el incio que empieza en 0 y va de 3 en 3, al igual que el nº registro, pero no es suma sino multiplicacion
		$sql="SELECT * FROM blog LIMIT $iniciolimite,$numeroRegistrosPagina";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			?>
			<tr>
				<td><?php echo $registro['titulo'] ?></td>
				<td>
					<a href="index.php?p=noticiasblog.php&accion=ver&id=<?php echo $registro['idNoticia']; ?>">Ver</a>
					<?php 
					if($_SESSION['conectado']){
					?>
					 - 
					<a href="index.php?p=noticiasblog.php&accion=borrar&id=<?php echo $registro['idNoticia']; ?>&numerodePagina<?php echo $numerodePagina ?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
					 - 
					<a href="index.php?p=noticiasblog.php&accion=modificar&id=<?php echo $registro['idNoticia']; ?>&numerodePagina<?php echo $numerodePagina ?>">Modificar</a>
					<?php } ?>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<hr>
			<ul class="pagination">
				
				<?php if($numerodePagina==0){ ?>
					<li class="disabled"><a href="#">&laquo;</a></li>
  				<?php } else { ?>
  					<li><a href="index.php?p=noticiasblog.php&accion=listado&numerodePagina=<?php echo ($numerodePagina-1); ?>">&laquo;</a></li>
  				<?php } ?>
  				
  				<?php //se calcula el nº total de paginas
  				$numeroTotalPaginas=ceil($numeroTotalRegistros/$numeroRegistrosPagina);
  				//el bucle es para poner los numeros de las páginas
  				for($numPagina=0;$numPagina<$numeroTotalPaginas;$numPagina++){
  					if($numerodePagina==$numPagina){
  						$activa='active';
  					}else{
  						$activa='';
  					}
	  				?>
					<li class="<?php echo $activa; ?>"><a href="index.php?p=noticiasblog.php&accion=listado&numerodePagina=<?php echo $numPagina; ?>"><?php echo ($numPagina+1); ?></a></li>
  					<?php
	  			}//fin del bucle for
  				?>
  				
  				<?php if($numerodePagina<$numeroTotalPaginas-1){ ?>
	 	 			<li><a href="index.php?p=noticiasblog.php&accion=listado&numerodePagina=<?php echo ($numerodePagina+1); ?>">&raquo;</a></li>
  				<?php } else { ?>
  					<li class="disabled"><a href="#">&laquo;</a></li>
  				<?php } ?>

			</ul>
		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////   VER   //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'ver':

		$id=$_GET['id'];
		$sql="SELECT * FROM blog WHERE idNoticia=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		?>
		<h4>
			<a href="index.php?p=noticiasblog.php&accion=listado">
				Volver
			</a>
		</h4>
		<hr>
		<article>
			<div class="jumbotron">
			<section>
				<h1><?php echo $registro['titulo']; ?></h1>
				<h3><?php echo $registro['contenido']; ?></h3>
				</section>
			<footer>
				<h4>Autor: <?php echo $registro['autor'];?> - a fecha de <?php echo $registro['fecha'];?></h4>
				</footer>
		</article>
		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////  BORRAR  //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'borrar':
		if($_SESSION['conectado']){
			$id=$_GET['id'];
			$sql="DELETE FROM blog WHERE idNoticia=$id";
			if($consulta=$conexion->query($sql)){
				//header('location:index.php?p=productos.php');
				header('Refresh: 2; url=index.php?p=noticiasblog.php&numerodePagina='.$numerodePagina);
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Realizado con exito
				</div>
				<?php
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
					<?php echo $sql; ?>
				</div>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  MODIFICAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'modificar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$titulo=$_POST['titulo'];
				$contenido=$_POST['contenido'];
				$autor=$_POST['autor'];
				$fecha=date('Y-m-d h:i:s');
				$idNoticia=$_POST['idNoticia'];

				if(isset($_POST['activado'])){
					$activado=1;
				}else{
					$activado=0;
				}

				$sql="UPDATE blog SET titulo='$titulo', contenido='$contenido', autor='$autor', fecha='$fecha' WHERE idNoticia='$idNoticia'";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 2; url=index.php?p=noticiasblog.php&numerodePagina='.$numerodePagina);
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{

				$id=$_GET['id'];
				$sql="SELECT * FROM blog WHERE idNoticia=$id";
				$consulta=$conexion->query($sql);
				$registro=$consulta->fetch_array();
			
				?>
				<form action="index.php?p=noticiasblog.php&accion=modificar&numerodePagina=<?php echo $numerodePagina; ?>" method="post">
					
					<div class="form-group">
						<label for="titulo">Titulo de Noticia:</label>
						<input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $registro['titulo']; ?>">
					</div>

					<div class="form-group">
						<label for="contenido">Descripcion de la noticia:</label>
						<input type="text" class="form-control" name="contenido" id="contenido" value="<?php echo $registro['contenido']; ?>">
					</div>

					<div class="form-group">
						<label for="precioProd">Autor de la noticia:</label>
						<input type="text" class="form-control" name="autor" id="autor" value="<?php echo $registro['autor']; ?>">
					</div>

					<input type="hidden" name="idNoticia" value="<?php echo $registro['idNoticia']; ?>">
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;


	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  INSERTAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$titulo=$_POST['titulo'];
				$contenido=$_POST['contenido'];
				$autor=$_POST['autor'];
				$fecha=date('Y-m-d h:i:s');

				$sql="INSERT INTO blog(titulo, contenido, autor, fecha)VALUES('$titulo', '$contenido', '$autor', '$fecha')";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 2; url=index.php?p=noticiasblog.php');
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{
			
				?>
				<form action="index.php?p=noticiasblog.php&accion=insertar" method="post">
					
					<div class="form-group">
						<label for="titulo">Nombre de la Noticia:</label>
						<input type="text" class="form-control" name="titulo" id="titulo">
					</div>

					<div class="form-group">
						<label for="contenido">Contenido de la Noticias:</label>
						<input type="text" class="form-control" name="contenido" id="contenido">
					</div>

					<div class="form-group">
						<label for="autor">Autor de la noticia:</label>
						<input type="text" class="form-control" name="autor" id="autor">
					</div>
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

} //Fin del switch($accion)
?>