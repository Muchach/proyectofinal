<h2>
	Tienda muchachal - El Poder Tiendil Muchachil 
</h2>
<hr>

<?php 
//se recoge en que pagina se está para mostrar unos resultados o otros

		if(isset($_GET['numerodePagina'])){
			$numerodePagina=$_GET['numerodePagina'];
		}else{
			$numerodePagina=0;
		}
 
//Este archivo va a recibir una accion a realizar
//si no recibe una accion, por defecto quiero LISTAR elementos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Pues dependiendo de $accion, la web hace una u otra cosa
switch($accion){
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=productos.php&accion=insertar">
				Insertar
			</a>
		</h4>
		<?php } ?>
		<hr>
		<table class="table table-striped table-hover">
		<tr>
			<th>Nombre del producto</th>
			<th>Acciones del producto</th>
		</tr>
		<?php  
		//hacemos un  listado con paginacion
		//se quiere saber el nº total de productos
		$sql="SELECT * FROM productos";
		$consulta=$conexion->query($sql);
		$numeroTotalRegistros=$consulta->num_rows;
		//se establece cuantos regiostros hay por pagina
		$numeroRegistrosPagina=5;//a mano
		
		//aqui estaba aantes el recoger en que pagina se esta que ahora está arrib		

		//hacer la consulta segun el numero de pagina para mostrar los productos de 3 en 3, en la pag 1, mostrar el producto 0,1,2 y así hasta acabar.
		$iniciolimite=$numerodePagina*$numeroRegistrosPagina;//como hay relacion entre los nº de pag y nº de registros se puede hacer en la multiplicaciond e variables, creando e que será el incio que empieza en 0 y va de 3 en 3, al igual que el nº registro, pero no es suma sino multiplicacion
		$sql="SELECT * FROM productos LIMIT $iniciolimite,$numeroRegistrosPagina";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			?>
			<tr>
				<td><?php echo $registro['nombreProd'] ?></td>
				<td>
					<a href="index.php?p=productos.php&accion=ver&id=<?php echo $registro['idProd']; ?>">Ver</a>
					<?php 
					if($_SESSION['conectado']){
					?>
					 - 
					<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $registro['idProd']; ?>&numerodePagina<?php echo $numerodePagina ?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>
					 - 
					<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $registro['idProd']; ?>&numerodePagina<?php echo $numerodePagina ?>">Modificar</a>
					<?php } ?>
				</td>
			</tr>
			<?php
		}
		?>
		</table>
		<hr>
			<ul class="pagination">
				
				<?php if($numerodePagina==0){ ?>
					<li class="disabled"><a href="#">&laquo;</a></li>
  				<?php } else { ?>
  					<li><a href="index.php?p=productos.php&accion=listado&numerodePagina=<?php echo ($numerodePagina-1); ?>">&laquo;</a></li>
  				<?php } ?>
  				
  				<?php //se calcula el nº total de paginas
  				$numeroTotalPaginas=ceil($numeroTotalRegistros/$numeroRegistrosPagina);
  				//el bucle es para poner los numeros de las páginas
  				for($numPagina=0;$numPagina<$numeroTotalPaginas;$numPagina++){
  					if($numerodePagina==$numPagina){
  						$activa='active';
  					}else{
  						$activa='';
  					}
	  				?>
					<li class="<?php echo $activa; ?>"><a href="index.php?p=productos.php&accion=listado&numerodePagina=<?php echo $numPagina; ?>"><?php echo ($numPagina+1); ?></a></li>
  					<?php
	  			}//fin del bucle for
  				?>
  				
  				<?php if($numerodePagina<$numeroTotalPaginas-1){ ?>
	 	 			<li><a href="index.php?p=productos.php&accion=listado&numerodePagina=<?php echo ($numerodePagina+1); ?>">&raquo;</a></li>
  				<?php } else { ?>
  					<li class="disabled"><a href="#">&laquo;</a></li>
  				<?php } ?>

			</ul>
		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////   VER   //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'ver':

		$id=$_GET['id'];
		$sql="SELECT * FROM productos WHERE idProd=$id";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		?>
		<h4>
			<a href="index.php?p=productos.php&accion=listado">
				Volver
			</a>
		</h4>
		<hr>
		<article>
			<div class="jumbotron">
			<section>
				<h1><?php echo $registro['nombreProd']; ?></h1>
				<h3><?php echo $registro['descripcionProd']; ?></h3>
			</section>
			<img src="images/<?php echo $registro['imagenProd']; ?>" class="img-responsive img-rounded">
			<footer>
				<h4>Precio <?php echo $registro['precioProd'];?> - disponibles <?php echo $registro['unidadesProd'];?></h4>
				</footer>
		</article>
		<?php

		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////  BORRAR  //////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'borrar':
		if($_SESSION['conectado']){
			$id=$_GET['id'];
			$sql="DELETE FROM productos WHERE idProd=$id";
			if($consulta=$conexion->query($sql)){
				//header('location:index.php?p=productos.php');
				header('Refresh: 2; url=index.php?p=productos.php&numerodePagina='.$numerodePagina);
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Realizado con exito
				</div>
				<?php
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
					<?php echo $sql; ?>
				</div>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  MODIFICAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'modificar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$nombreProd=$_POST['nombreProd'];
				$descripcionProd=$_POST['descripcionProd'];
				$precioProd=$_POST['precioProd'];
				$unidadesProd=$_POST['unidadesProd'];
				$idProd=$_POST['idProd'];
				$nombreImagen=$_FILES['imagenProd']['name'];
				move_uploaded_file($_FILES ['imagenProd']['tmp_name'], 'images/'.$nombreImagen);

				if(isset($_POST['activado'])){
					$activado=1;
				}else{
					$activado=0;
				}

				$sql="UPDATE productos SET nombreProd='$nombreProd', descripcionProd='$descripcionProd', precioProd='$precioProd', unidadesProd='$unidadesProd', activado=$activado, imagenProd='$nombreImagen' WHERE idProd='$idProd'";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 20; url=index.php?p=productos.php&numerodePagina='.$numerodePagina);
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{

				$id=$_GET['id'];
				$sql="SELECT * FROM productos WHERE idProd=$id";
				$consulta=$conexion->query($sql);
				$registro=$consulta->fetch_array();
			
				?>
				<form action="index.php?p=productos.php&accion=modificar&numerodePagina=<?php echo $numerodePagina; ?>" method="post" enctype="multipart/form-data">
					
					<div class="form-group">
						<label for="nombreProd">Nombre del producto:</label>
						<input type="text" class="form-control" name="nombreProd" id="nombreProd" value="<?php echo $registro['nombreProd']; ?>">
					</div>

					<div class="form-group">
						<label for="descripcionProd">Descripcion del producto:</label>
						<input type="text" class="form-control" name="descripcionProd" id="descripcionProd" value="<?php echo $registro['descripcionProd']; ?>">
					</div>

					<div class="form-group">
						<label for="precioProd">Precio del producto:</label>
						<input type="text" class="form-control" name="precioProd" id="precioProd" value="<?php echo $registro['precioProd']; ?>">
					</div>

					<div class="form-group">
						<label for="unidadesProd">Unidades del producto:</label>
						<input type="text" class="form-control" name="unidadesProd" id="unidadesProd" value="<?php echo $registro['unidadesProd']; ?>">
					</div>

					<div class="form-group">
						<label for="imagenProd">Imagen de producto</label>
						<input type="file" name="imagenProd">
					</div>					
					
					
					<?php
					if($registro['activado']==1){
						$checked='checked';
					}else{
						$checked='';
					}
					?>

					<div class="form-group">
						<label for="activado">Producto Activado</label>
						<input type="checkbox" class="form-control" name="activado" id="activado" <?php echo $checked; ?>>
					</div>				

					<input type="hidden" name="idProd" value="<?php echo $registro['idProd']; ?>">
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;


	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	//////////////////  INSERTAR  /////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
			if(isset($_POST['enviar'])){

				$nombreProd=$_POST['nombreProd'];
				$descripcionProd=$_POST['descripcionProd'];
				$precioProd=$_POST['precioProd'];
				$unidadesProd=$_POST['unidadesProd'];
				$nombreImagen=$_FILES['imagenProd']['name'];
				move_uploaded_file($_FILES ['imagenProd']['tmp_name'], 'images/'.$nombreImagen);
				if(isset($_POST['activado'])){
					$activado=1;
				}else{
					$activado=0;
				}

				$sql="INSERT INTO productos(nombreProd, descripcionProd, precioProd, unidadesProd, activado, imagenProd)VALUES('$nombreProd', '$descripcionProd', '$precioProd', '$unidadesProd', '$activado', '$nombreImagen')";

				if($consulta=$conexion->query($sql)){
					//header('location:index.php?p=productos.php');
					header('Refresh: 2; url=index.php?p=productos.php');
					?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Realizado con exito
					</div>
					<?php
				}else{
					?>
					<div class="alert alert-danger">
						<strong>ERROR!!</strong> 
						<?php echo $sql; ?>
					</div>
					<?php
				}

			}else{
			
				?>
				<form action="index.php?p=productos.php&accion=insertar" method="post" enctype="multipart/form-data">
					
					<div class="form-group">
						<label for="nombreProd">Nombre del producto:</label>
						<input type="text" class="form-control" name="nombreProd" id="nombreProd">
					</div>

					<div class="form-group">
						<label for="descripcionProd">Descripcion del producto:</label>
						<input type="text" class="form-control" name="descripcionProd" id="descripcionProd">
					</div>

					<div class="form-group">
						<label for="precioProd">Precio del producto:</label>
						<input type="text" class="form-control" name="precioProd" id="precioProd">
					</div>

					<div class="form-group">
						<label for="unidadesProd">Unidades del producto:</label>
						<input type="text" class="form-control" name="unidadesProd" id="unidadesProd">
					</div>

					<div class="form-group">
						<label for="activado">Producto Activado</label>
						<input type="checkbox" class="form-control" name="activado" id="activado" checked>
					</div>
					<div class="form-group">
						<label for="imagen">Imagen de producto</label>
						<input type="file" name="imagenProd">
					</div>					
					
					<input type="submit" class="form-control" name="enviar" value="enviar">

				</form>
				<?php
			}
		}else{
			echo 'No tienes permisos para estar aqui. Logueate....!!';
		} //fin del else de if($_SESSION['conectado'])
		break;

} //Fin del switch($accion)
?>